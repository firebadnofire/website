document.addEventListener('DOMContentLoaded', () => {
  const codeBox = document.getElementById('code-box');
  const copyBtn = document.getElementById('copy-btn');

  function copyToClipboard(element) {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(element);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand("copy");
  }

  copyBtn.addEventListener('click', () => {
    copyToClipboard(codeBox);
    alert('Code copied to clipboard!');
  });
});
