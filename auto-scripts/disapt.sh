#!/bin/bash
main() {
echo "deb https://palfrey.github.io/discord-apt/debian/ ./" | sudo tee /etc/apt/sources.list.d/discord.list > /dev/null
sudo curl -o /etc/apt/trusted.gpg.d/discord-apt.gpg.asc https://palfrey.github.io/discord-apt/discord-apt.gpg.asc
sudo apt-get update
apt-cache show discord
}
main
