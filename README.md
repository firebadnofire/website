# Website

This git repo (<a href="https://codeberg.org/firebadnofire/website">website</a>) is a full copy of <a href="https://archuser.org/site/">archuser.org/site/</a>.

Why only /site/? Because the rest would be too big to store on a remote server and this is the only part I WANT to be redistributable. 
